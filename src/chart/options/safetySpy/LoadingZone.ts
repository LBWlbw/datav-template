import * as echarts from 'echarts/core';

const dataX = ['1月', '2月', '3月', '4月', '5月', '6月'];
const dataY = [98, 38, 48, 35, 92, 28];
const dataY1 = [400, 500, 300, 300, 300, 400];

const loadingzone = (data: any) => {
    const defaultConfig = {
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'none'
            },
            formatter(params: any) {
                return (
                    `${dataX[params[0].dataIndex]}<br/>增速：${dataY[params[0].dataIndex]}%` +
                    `<br> 装卸量：${dataY1[params[0].dataIndex]}`
                );
            }
        },
        grid: {
            top: '25%',
            bottom: '0',
            left: '15',
            right: '10',
            containLabel: true
        },
        legend: [
            {
                data: ['装卸量（吨）', { name: '每月环比增速', itemStyle: { color: '#ffffff' } }],
                left: 'center',
                top: '0',
                textStyle: {
                    color: '33FFFF'
                },
                itemWidth: 18,
                itemHeight: 8,
                itemGap: 20
            }
        ],
        xAxis: {
            type: 'category',
            data: dataX,
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
                color: '#FFFFFF',
                fontSize: 10
            }
        },

        yAxis: [
            {
                type: 'value',
                name: '装卸量',
                nameTextStyle: {
                    color: '#FFFFFF',
                    fontSize: 12,
                    padding: [0, 25, 0, 0]
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: 'rgba(0, 142, 114, 0.33)',
                        type: 'dashed'
                    }
                },
                min: '0',
                position: 'left',
                axisLabel: {
                    color: '#FFFFFF',
                    fontSize: 10
                },

                axisLine: {
                    show: false
                }
            },
            {
                type: 'value',
                name: '增速',
                nameTextStyle: {
                    color: '#FFFFFF',
                    fontSize: 12,
                    padding: [0, 0, 0, 40]
                },
                max: '100',
                min: '0',
                position: 'right',
                axisLine: {
                    show: false
                },
                splitLine: {
                    show: false
                },
                axisLabel: {
                    show: true,
                    formatter: '{value} %', // 右侧Y轴文字显示
                    color: '#FFFFFF',
                    fontSize: 10
                }
            }
        ],
        series: [
            {
                name: '装卸量（吨）',
                type: 'bar',
                barWidth: '8',
                itemStyle: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                        {
                            offset: 0,
                            color: 'rgba(102, 208, 230, 1)'
                        },
                        {
                            offset: 1,
                            color: 'rgba(70, 127, 251, 1)'
                        }
                    ]),
                    borderRadius: 6
                },
                data: dataY1
            },
            {
                name: '每月环比增速',
                type: 'line',
                yAxisIndex: 1, // 使用的 y 轴的 index，在单个图表实例中存在多个 y轴的时候有用
                smooth: false, // 平滑曲线显示

                symbol: 'circle', // 标记的图形为实心圆
                symbolSize: 6, // 标记的大小
                itemStyle: {
                    color: 'rgba(40, 190, 198, 1)',
                    borderColor: 'rgba(255, 255, 255, 1)', // 圆点透明 边框
                    borderWidth: 2
                },
                lineStyle: {
                    color: 'rgba(40, 190, 198, 1)'
                },

                data: dataY
            }
        ]
    };

    const opt = { ...defaultConfig };
    return opt;
};

export default {
    loadingzone
};
