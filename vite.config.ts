import { resolve } from 'path';
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import AutoImport from 'unplugin-auto-import/vite';
import Components from 'unplugin-vue-components/vite';
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers';
import OptimizationPersist from 'vite-plugin-optimize-persist';
import PkgConfig from 'vite-plugin-package-config';

export default defineConfig({
    resolve: {
        alias: {
            '@': resolve(__dirname, 'src'),
            '@img': resolve(__dirname, 'src/assets/image')
        },
        extensions: ['.js', '.json', '.ts', '.tsx']
    },
    css: {
        preprocessorOptions: {
            scss: {
                additionalData: `@use "./src/styles/scss/global.scss" as *;`
            }
        }
    },
    plugins: [
        vue(),
        AutoImport({
            resolvers: [ElementPlusResolver()]
        }),
        Components({
            resolvers: [ElementPlusResolver()],
            deep: true,
            dirs: ['src/components'],
            extensions: ['vue'],
            directoryAsNamespace: true,
            dts: 'components.d.ts'
        }),
        OptimizationPersist(),
        PkgConfig()
    ],
    logLevel: 'info',
    // base: '/', // 打包路径
    clearScreen: false, // 不会清空上一次控制台打印信息
    // sourcemap: true, // 开启
    build: {
        target: 'modules',
        // polyfillDynamicImport: true, // 自动注入 动态导入 polyfill
        outDir: 'dist',
        // assetsDir: '/static/img',
        assetsInlineLimit: 8192, // 小于 8kb 的导入或引用资源将内联为 base64 编码
        cssCodeSplit: false, // 在异步 chunk 中导入的 CSS 将内联到异步 chunk 本身，并在其被加载时插入
        sourcemap: false, // 构建后是否生成 source map 文件
        terserOptions: {
            // 生产环境移除console
            compress: {
                drop_console: false,
                drop_debugger: true
            }
        },
        rollupOptions: {
            output: {
                manualChunks: {
                    // nprogress: ['nprogress'],
                    vue: ['vue']
                },
                // 分包
                chunkFileNames: 'static/js/[name]-[hash].js',
                entryFileNames: 'static/js/[name]-[hash].js',
                assetFileNames: 'static/[ext]/[name]-[hash].[ext]'
            }
        },
        minify: 'terser',
        chunkSizeWarningLimit: 800 // chunk 大小警告的限制
    },
    server: {
        host: '0.0.0.0',
        port: 3200
    }
});
