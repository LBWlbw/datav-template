/** 预警详情 */
import * as echarts from 'echarts/core';

const warnInfoLine = (data: any) => {
    const defaultConfig = {
        tooltip: {
            trigger: 'axis',
            backgroundColor: 'rgba(0,0,0,0.3)',
            borderColor: 'rgba(0,0,0,0.3)',

            textStyle: {
                color: '#FFF'
            },

            axisPointer: {
                type: 'line'
            }
        },
        grid: {
            top: '20%',
            left: '6%',
            right: '6%',
            bottom: '18%'
        },
        xAxis: [
            {
                type: 'category',
                axisLine: {
                    show: true,
                    lineStyle: { color: '#dddddd' }
                },
                axisLabel: { color: '#CCCCCC', fontSize: 12 },
                splitLine: { show: false },
                axisTick: { show: false },
                boundaryGap: false,
                data: ['09:20:01', '09:20:01', '09:20:01', '09:20:01', '09:20:01', '09:20:01', '09:20:01']
            }
        ],

        yAxis: [
            {
                max: 500,
                name: '压力（mPa）',
                nameTextStyle: { color: 'rgba(250,250,250,.5)', padding: [10, 0, 10, 12] },
                type: 'value',
                splitNumber: 4,
                axisLabel: {
                    formatter: '{value}',
                    color: '#ADC1F8',
                    fontSize: 12
                },
                axisLine: {
                    lineStyle: {
                        color: 'rgba(255,255,255,0.45)'
                    }
                },
                axisTick: {
                    show: false
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: 'rgba(255,255,255,0.15)',
                        type: 'dashed'
                    }
                }
            }
        ],
        series: [
            {
                name: '用户量',
                type: 'line',
                showAllSymbol: true,
                symbol: 'circle',
                symbolSize: 10,
                lineStyle: { normal: { color: '#3158FF', width: 3 } },

                itemStyle: {
                    color: '#527FFF',
                    borderColor: '#fff',
                    borderWidth: 3
                },

                areaStyle: {
                    normal: {
                        color: new echarts.graphic.LinearGradient(
                            0,
                            0,
                            0,
                            1,
                            [
                                { offset: 0, color: 'rgba(49,122,255,.3)' },
                                { offset: 1, color: 'rgba(159, 88, 255, 0)' }
                            ],
                            false
                        )
                    }
                },
                data: [210, 150, 380, 210, 230, 50, 200]
            }
        ]
    };

    const opt = { ...defaultConfig };
    return opt;
};

export default {
    warnInfoLine
};
