import { App } from 'vue';
import ChartPanel from './index.vue';

const modulesFiles = import.meta.globEager('./options/**/*.ts');
// eslint-disable-next-line import/no-mutable-exports
let modules: any = {};
Object.keys(modulesFiles).forEach(item => {
    const mod = modulesFiles[item];
    const file = mod.default;
    modules = { ...modules, ...file };
});

export function ChartPanelMount(app: App) {
    app.component(ChartPanel.name, ChartPanel);
}
const EchartsMethods = modules;
export { EchartsMethods };
