import { createVNode, render, h } from 'vue';
import type { Component } from 'vue';
import Popconfirm from './index.vue';

/** - 弹框渲染函数
 *
 *  - data: props参数
 *  - slotDefault: 默认插槽
 */
const div = document.createElement('div');
div.setAttribute('class', 'my-confirm');

export default function renderPopconfirmComponent(data: PopConfirm.Modal, slotDefault: Component): void {
    div.style.position = 'fixed';
    div.style.top = '0';
    div.style.left = '0';
    div.style.width = '100%';
    div.style.height = '100%';
    div.style.background = 'rgba(0,0,0,0.3)';
    div.style.zIndex = '3';
    document.body.appendChild(div);
    const { showPop, title, w, zIndex, isChildren } = data;
    const app = createVNode(
        Popconfirm,
        {
            'onUpdate:showPop': () => {
                render(null, div);
                div.removeAttribute('style');
            },
            title,
            w,
            zIndex,
            showPop
        },
        {
            default: () => {
                return createVNode(slotDefault, {
                    onHiden: () => {
                        render(null, div);
                        div.removeAttribute('style');
                    },
                    onShowChildren: (e: any) => {
                        const childrenDiv = document.createElement('div');
                        childrenDiv.setAttribute('class', 'childrenDiv');
                        childrenDiv.style.position = 'fixed';
                        childrenDiv.style.top = '0';
                        childrenDiv.style.left = '0';
                        childrenDiv.style.width = '100%';
                        childrenDiv.style.height = '100%';
                        childrenDiv.style.zIndex = '30';
                        childrenDiv.style.display = 'flex';
                        childrenDiv.style.alignItems = 'center';
                        childrenDiv.style.justifyContent = 'center';
                        document.body.appendChild(childrenDiv);

                        const childrenDialog = createVNode(
                            Popconfirm,
                            {
                                'onUpdate:showPop': () => {
                                    render(null, childrenDiv);
                                    childrenDiv.removeAttribute('style');
                                },
                                title: data.childrenProps?.title,
                                w: data.childrenProps?.w,
                                zIndex: data.childrenProps?.zIndex,
                                isChildren,
                                showPop: data.childrenProps?.showPop
                            },
                            {
                                default: () =>
                                    createVNode(data.childrenProps?.compoent, {
                                        onHidens: () => {
                                            render(null, childrenDiv);
                                            render(null, div);
                                            childrenDiv.removeAttribute('style');
                                            div.removeAttribute('style');
                                        }
                                    })
                            }
                        );
                        render(childrenDialog, childrenDiv);
                    }
                });
            }
        }
    );
    render(app, div);
}
