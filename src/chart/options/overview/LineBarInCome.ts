import * as echarts from 'echarts/core';

const dataX = ['01月', '02月', '03月', '04月', '05月', '06月', '07月', '08月', '09月'];
const dataY = [98, 38, 48, 35, 92, 28, 93, 85, 15];
const dataY1 = [400, 500, 300, 300, 300, 400, 400, 400, 300, 30];

const LineBarInCome = (data: any) => {
    const defaultConfig = {
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'none'
            }
            // formatter(params: any): string {
            //     return (
            //         `${dataX[params[0].dataIndex]}<br/>满意度：${dataY[params[0].dataIndex]}%` +
            //         `<br> 处理量：${dataY1[params[0].dataIndex]}`
            //     );
            // }
        },
        grid: {
            top: '20%',
            bottom: '10%',
            left: '0%',
            right: '5%',
            containLabel: true
        },
        legend: {
            right: '15',
            top: '2.4%',
            textStyle: {
                padding: [4, 0, 0, 0],
                color: '#B7BDBF',
                fontSize: 10
            },
            data: [
                {
                    name: '回访数量',
                    itemStyle: {
                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                            {
                                offset: 0,
                                color: 'rgba(0,104,220,1)'
                            },
                            {
                                offset: 1,
                                color: 'rgba(0,235,177,1)'
                            }
                        ])
                    }
                },
                { name: '满意率', itemStyle: { color: '#FFDA30' } }
            ],
            itemWidth: 15,
            itemHeight: 10,
            itemGap: 10
        },
        xAxis: {
            type: 'category',
            data: dataX,
            axisLine: {
                lineStyle: {
                    color: 'rgba(66, 192, 255, .3)'
                }
            },
            axisTick: {
                show: false // 隐藏X轴刻度
            },
            axisLabel: {
                interval: 0,
                color: '#B7BDBF',
                fontSize: 10
            }
        },

        yAxis: {
            type: 'value',
            axisLine: {
                lineStyle: {
                    color: 'rgba(66, 192, 255, .3)'
                }
            },
            splitLine: {
                show: true,
                lineStyle: {
                    color: 'rgba(0,142,114,.2)'
                }
            },
            axisLabel: {
                show: true,
                fontSize: 10,
                color: '#B3BCB9'
            }
        },
        series: [
            {
                name: '回访数量',
                type: 'bar',
                barWidth: '10px',
                itemStyle: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                        {
                            offset: 0,
                            color: 'rgba(0,104,220,.3)'
                        },
                        {
                            offset: 1,
                            color: 'rgba(0,235,177,.3)'
                        }
                    ]),
                    borderColor: 'rgba(0,104,220,1)'
                },
                data: dataY1
            },
            {
                name: '满意率',
                type: 'line',
                smooth: false, // 平滑曲线显示
                symbol: 'circle', // 标记的图形为实心圆
                symbolSize: 5, // 标记的大小
                itemStyle: {
                    color: '#FFDA30',
                    borderWidth: 5
                },
                lineStyle: {
                    color: '#FFDA30'
                },

                data: dataY
            }
        ]
    };

    const opt = { ...defaultConfig };
    return opt;
};

export default {
    LineBarInCome
};
