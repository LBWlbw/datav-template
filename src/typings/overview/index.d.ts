/** - 园区信息 */
declare namespace Overview {
    /** SiderLeft tab图标 */
    interface ParkTabIcon {
        /** 图片名称 */
        img: string;
        /** 文字信息 */
        title: string;
    }
}
