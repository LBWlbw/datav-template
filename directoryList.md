```
    |-- .eslintignore  eslint不包含文件
    |-- .eslintrc.js   eslint配置
    |-- .gitignore     git不提交的文件
    |-- .prettierrc.js 代码美化配置
    |-- auto-imports.d.ts 不用管
    |-- components.d.ts   全局组件
    |-- index.html
    |-- package-lock.json
    |-- package.json
    |-- README.md
    |-- tsconfig.json    ts配置
    |-- tsconfig.node.json 不用管
    |-- vite.config.ts   vite脚手架配置
    |-- src
        |-- assets  静态资源
        |   |-- font
        |   |   |-- pmzd.ttf
        |   |-- image
        |-- chart 图表配置
        |   |-- index.ts
        |   |-- index.vue
        |   |-- renderer.js 按需加载图表配置
        |   |-- options 每个模块的组件配置
        |       |-- overview
        |-- components
        |   |-- common 公共组件
        |   |   |-- borderLarge
        |   |   |   |-- index.vue
        |   |   |-- PageScreen  每个页面的Sider左右组件，参考overview | securityCheck文件代码
        |   |   |   |-- index.ts
        |   |   |   |-- overview
        |   |   |   |   |-- OverviewSiderLeft.vue
        |   |   |   |   |-- OverviewSiderRight.vue
        |   |   |   |-- securityCheck
        |   |   |       |-- SecurityCheckSiderLeft.vue
        |   |   |       |-- SecurityCheckSiderRight.vue
        |   |   |-- TitleComponent
        |   |       |-- index.vue
        |   |-- custom  组定义组件
        |       |-- overview 每个页面的Sider模块组件，参考overview | securityCheck文件代码
        |           |-- SiderLeft
        |           |   |-- EnterpriseStatisticsEchart.vue
        |           |   |-- ParkEconomicsInfo.vue
        |           |   |-- ParkIncomeEchart.vue
        |           |   |-- ParkMoreInfo.vue
        |           |   |-- ParkProfitEchart.vue
        |           |-- SiderRight
        |               |-- EquipmentStatistics.vue
        |               |-- FacilityStatistics.vue
        |               |-- MonitoringpointStatistics.vue
        |               |-- PipeGalleryStatistics.vue
        |-- layouts  公共布局
        |   |-- BasicLayout 呈现页面
        |   |   |-- index.vue
        |   |-- common 公共布局组件
        |       |-- GlobalContent.vue
        |       |-- GlobalFooter.vue
        |       |-- GlobalHeader.vue
        |       |-- GlobalSider.vue
        |       |-- GlobalSiderLeft.vue
        |       |-- GlobalSiderRight.vue
        |       |-- index.ts
        |-- router
        |-- store pinia状态管理
        |   |-- index.ts
        |   |-- modules
        |       |-- index.ts
        |       |-- CheckSiderView
        |           |-- index.ts
        |-- styles style样式配置
        |   |-- scss
        |       |-- global.scss 全局样式配置
        |-- typings ts声明文件
        |   |-- env.d.ts
        |   |-- overview
        |   |   |-- index.d.ts
        |   |-- sider
        |       |-- index.d.ts
        |-- utils
        |-- views
```
