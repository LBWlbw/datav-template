import layoutFooterOverviewActive from '@img/layoutFooterOverviewActive.png';
import layoutFooterOverview from '@img/layoutFooterOverview.png';
import layoutFooterSafesupervisi from '@img/layoutFooterSafesupervisi.png';
import layoutFooterSafesupervisiActive from '@img/layoutFooterSafesupervisiActive.png';
import layoutFooterEnvironment from '@img/layoutFooterEnvironment.png';
import layoutFooterEnvironmentActive from '@img/layoutFooterEnvironmentActive.png';
import layoutFooterSecurity from '@img/layoutFooterSecurity.png';
import layoutFooterSecurityActive from '@img/layoutFooterSecurityActive.png';
import layoutFooterFirefight from '@img/layoutFooterFirefight.png';
import layoutFooterFirefightActive from '@img/layoutFooterFirefightActive.png';
import layoutFooterEnergy from '@img/layoutFooterEnergy.png';
import layoutFooterEnergyActive from '@img/layoutFooterEnergyActive.png';
import layoutFooterEmergency from '@img/layoutFooterEmergency.png';
import layoutFooterEmergencyActive from '@img/layoutFooterEmergencyActive.png';
import layoutFooterStatistics from '@img/layoutFooterStatistics.png';
import layoutFooterStatisticsActive from '@img/layoutFooterStatisticsActive.png';

export {
    layoutFooterOverviewActive,
    layoutFooterOverview,
    layoutFooterSafesupervisi,
    layoutFooterSafesupervisiActive,
    layoutFooterEnvironment,
    layoutFooterEnvironmentActive,
    layoutFooterSecurity,
    layoutFooterSecurityActive,
    layoutFooterFirefight,
    layoutFooterFirefightActive,
    layoutFooterEnergy,
    layoutFooterEmergency,
    layoutFooterEmergencyActive,
    layoutFooterStatistics,
    layoutFooterEnergyActive,
    layoutFooterStatisticsActive
};
