import GlobalHeader from './GlobalHeader.vue';
import GlobalCesium from './GlobalCesium.vue';
import GlobalSider from './GlobalSider.vue';
import GlobalFooter from './GlobalFooter.vue';
import GlobalCenter from './GlobalCenter.vue';
import GlobalSiderRight from './GlobalSiderRight.vue';
import GlobalSiderLeft from './GlobalSiderLeft.vue';

export { GlobalHeader, GlobalCesium, GlobalSider, GlobalFooter, GlobalCenter, GlobalSiderLeft, GlobalSiderRight };
