declare namespace ViewSider {
    /** Sider视图
     *  - OverView: 总览
     *  - SecurityCheck: 安监
     *  - GreenInitiative: 环保（有毒有害气体）
     *  - GreenInitiativeWaterQuality: 环保(水质&固废)
     *  - Security: 安防
     *  - FireControl: 消防
     *  - Energy: 能源
     *  - Emergency: 应急
     *  - EmergencyTab:应急（启动应急）
     *  - Statistics: 统计
     */
    type ViewStatus =
        | 'OverView'
        | 'SecurityCheck'
        | 'GreenInitiative'
        | 'GreenInitiativeWaterQuality'
        | 'Security'
        | 'FireControl'
        | 'Energy'
        | 'Emergency'
        | 'EmergencyTab'
        | 'Statistics';
    /** 模块 */
    type PickState =
        | 'OverView'
        | 'SecurityCheck'
        | 'GreenInitiative'
        | 'GreenInitiativeWaterQuality'
        | 'Security'
        | 'Energy'
        | 'Emergency'
        | 'EmergencyTab'
        | 'Statistics';

    type ViewsFilter = {
        [k in ViewSider.ViewStatus]: Component;
    };
    /** 命中当前视图 */
    type HitViewStatus = Pick<ViewsFilter, PickState>;

    /** 左侧list数据类型 */
    interface SiderLeftListDataInfo {
        name: string;
        children?: SiderLeftListDataInfo[] | undefined;
    }
}
