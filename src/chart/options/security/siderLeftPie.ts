import * as echarts from 'echarts/core';

const siderLeftPie = (data: any) => {
    const defaultConfig = {
        tooltip: {
            show: false
        },
        legend: {
            show: false
        },
        title: {
            text: `道闸`,
            left: 'center',
            top: '37%', // top待调整
            textStyle: {
                color: '#D3E9FC',
                fontSize: 12
            },
            subtext: '占比',
            subtextStyle: {
                color: '#D3E9FC',
                fontSize: 12
            },
            itemGap: 4
        },
        grid: {},
        series: [
            {
                name: 'Access From',
                type: 'pie',
                radius: ['50%', '70%'],
                avoidLabelOverlap: false,
                left: 4,
                label: {
                    show: false,
                    position: 'center'
                },
                labelLine: {
                    show: false
                },
                data: data.pieData
            }
        ]
    };

    // var currentIndex = 0;
    // var timer = 0;
    // function animate() {
    //   timer = setInterval(function () {
    //     var dataLen = option.series[0].data.length;
    //       // 取消之前高亮的图形
    //     myChart.dispatchAction({
    //       type: 'downplay',
    //       seriesIndex: 0,
    //       dataIndex: currentIndex,
    //     });
    //     currentIndex = (currentIndex + 1) % dataLen;
    //     // 高亮当前图形
    //     myChart.dispatchAction({
    //         type: 'highlight',
    //         seriesIndex: 0,
    //         dataIndex: currentIndex,
    //     });
    //   }, 1500);
    // }
    // animate();

    // myChart.on("mouseover", function(e) {
    //   clearInterval(timer);
    //   // 取消之前高亮的图形
    //   myChart.dispatchAction({
    //     type: 'downplay',
    //     seriesIndex: 0,
    //   });
    //   myChart.dispatchAction({
    //     type: 'highlight',
    //     dataIndex: e.dataIndex
    //   });
    // });

    // myChart.on("mouseout", function(e) {
    //   currentIndex = e.dataIndex;
    //   animate();
    // });

    const opt = { ...defaultConfig };
    return opt;
};

export default {
    siderLeftPie
};
