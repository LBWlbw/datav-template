import { createApp } from 'vue';
// import { setupRouter } from '@/router';
import { setupStore } from '@/store';
import { ChartPanelMount } from '@/chart/index';
// import 'amfe-flexible/index.js';
import '@/utils/rem';
import App from './App.vue';

async function setupApp() {
    const app = createApp(App);
    // 挂载pinia状态
    setupStore(app);
    // 挂在Echarts全局组件
    ChartPanelMount(app);
    // 挂载路由
    // await setupRouter(app);
    app.mount('#app');
}

setupApp();
