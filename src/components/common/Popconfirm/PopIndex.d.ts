declare namespace PopConfirm {
    /** 弹框宽度，尽量用响应式单位 */
    interface Modal {
        w?: string;
        /* 标题 */
        title?: string;
        /* 显示层级 */
        zIndex?: number;
        /** 显示弹框 */
        showPop?: boolean;
        /* 嵌套弹框 */
        isChildren?: boolean;
        /** 嵌套弹框的属性 */
        childrenProps?: Modal & { compoent: any };
    }
}
