import * as echarts from 'echarts/core';

const data1 = [
    {
        name: '安监',
        value: 175.17
    },
    {
        name: '安防',
        value: 148.35
    },
    {
        name: '环保',
        value: 95.36
    },
    {
        name: '能耗',
        value: 56
    },
    {
        name: '车辆',
        value: 45
    }
];
const xAxisData: any = [];
const seriesData1: any = [];
let sum = 0;
data1.forEach(item => {
    xAxisData.push(item.name);
    seriesData1.push(item.value);
    sum += item.value;
});

const MonitoringStatistics = (data: any) => {
    const defaultConfig = {
        grid: {
            top: '20',
            bottom: '20',
            right: '10'
        },
        xAxis: {
            data: xAxisData,
            axisTick: {
                show: false
            },
            axisLine: {
                show: true,
                lineStyle: {
                    color: 'rgba(0, 142, 114, 0.1)'
                }
            },
            axisLabel: {
                show: true,
                align: 'center',
                fontSize: 10,
                color: '#B7BDBF'
            },
            interval: 0
        },
        yAxis: {
            type: 'value',
            nameTextStyle: {
                color: '#FFFFFF',
                fontSize: 12
            },
            splitLine: {
                show: true,
                lineStyle: {
                    color: 'rgba(0, 142, 114, 0.1)'
                }
            },
            axisTick: {
                show: false
            },
            axisLine: {
                show: true
            },
            axisLabel: {
                show: true,
                fontSize: 10,
                color: '#B7BDBF'
            }
        },
        series: [
            {
                name: '柱顶部',
                type: 'pictorialBar',
                symbolSize: [26, 10],
                symbolOffset: [0, -5],
                z: 12,
                itemStyle: {
                    color: 'rgba(33, 243, 255, 0.7)'
                },
                label: {
                    show: false,
                    position: 'top',
                    fontSize: 16
                },
                symbolPosition: 'end',
                data: seriesData1
            },
            {
                name: '柱底部',
                type: 'pictorialBar',
                symbolSize: [26, 10],
                symbolOffset: [0, 5],
                z: 12,
                itemStyle: {
                    color: 'rgba(31, 97, 234, 0.4)'
                },
                data: seriesData1
            },
            {
                type: 'bar',
                itemStyle: {
                    color: function (params: any) {
                        return new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                            {
                                offset: 1,
                                color: 'rgba(34, 68, 172, 0.35)'
                            },
                            {
                                offset: 0,
                                color: 'rgba(0, 183, 255, 1)'
                            }
                        ]);
                    },
                    opacity: 0.8
                },
                label: {
                    show: true,
                    position: 'top',
                    fontSize: 12,
                    color: '#FFFFFF'
                },
                z: 16,
                silent: true,
                barWidth: 26,
                barGap: '-100%', // Make series be overlap
                data: seriesData1
            }
        ]
    };

    const opt = { ...defaultConfig };
    return opt;
};

export default {
    MonitoringStatistics
};
