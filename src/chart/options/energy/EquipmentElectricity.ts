/** 近1年环保设备用电 */
import * as echarts from 'echarts/core';

const EquipmentElectricity = (data: any) => {
    const defaultConfig = {
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                lineStyle: {
                    color: 'rgba(8, 195, 171, 1)'
                }
            },
            formatter(params: any) {
                return `${params[0].name}月<br/><span style='display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:rgba(36,207,233,0.9)'></span>${params[0].seriesName}：<span>${params[0].data}度</span><br/>`;
            }
        },
        legend: {
            show: false
        },
        grid: {
            top: '25%',
            left: '40',
            right: '15%',
            bottom: '14%'
        },
        xAxis: {
            name: '(月份)',
            nameTextStyle: {
                fontSize: 10,
                color: '#B7BDBF',
                verticalAlign: 'top'
            },
            type: 'category',
            data: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
            axisLabel: {
                color: '#B7BDBF',
                fontSize: 10
            },
            axisTick: {
                // 坐标轴刻度相关设置。
                show: false
            },
            axisLine: {
                show: false
            },
            splitLine: {
                // 坐标轴在 grid 区域中的分隔线。
                show: false
            }
        },
        yAxis: [
            {
                name: '单位：度',
                nameTextStyle: {
                    fontSize: 10,
                    color: '#B7BDBF',
                    padding: [20, 30, 0, 0]
                },
                type: 'value',
                axisLabel: {
                    color: '#B7BDBF',
                    fontSize: 10
                },
                axisLine: {
                    show: false
                },
                axisTick: {
                    show: false
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        type: 'dashed',
                        color: 'rgba(0, 142, 114, 0.33)'
                    }
                }
            }
        ],
        series: [
            {
                name: '用电量',
                type: 'line',
                smooth: true,
                showSymbol: false,
                symbolSize: 8,
                itemStyle: {
                    color: 'rgba(8, 195, 171, 1)',
                    shadowBlur: 20,
                    borderColor: 'rgba(255, 255, 255, 1)',
                    borderWidth: 1
                },
                areaStyle: {
                    color: new echarts.graphic.LinearGradient(1, 0, 0, 0, [
                        {
                            offset: 0,
                            color: 'rgba(0, 104, 220, .6)'
                        },
                        {
                            offset: 1,
                            color: 'rgba(0, 227, 192, .6)'
                        }
                    ])
                },
                lineStyle: {
                    color: new echarts.graphic.LinearGradient(1, 0, 0, 0, [
                        {
                            offset: 0,
                            color: 'rgba(0, 104, 220, .6)'
                        },
                        {
                            offset: 1,
                            color: 'rgba(0, 227, 192, .6)'
                        }
                    ])
                },
                data: [0, 100, 200, 99, 100, 12, 102, 112, 147, 76, 38, 98]
            }
        ]
    };

    const opt = { ...defaultConfig };
    return opt;
};

export default {
    EquipmentElectricity
};
