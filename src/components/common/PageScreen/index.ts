import { Component, defineComponent } from 'vue';
/**
 *  - 批量导入PageScreen/xx/Xxxx.vue文件
 *
 *  规则:
 *    1.  文件夹名称大小写不敏感.
 *    2. .vue文件首字母必须大写,且以文件名为导出名
 *
 *  例如:
 *    PageScreen/xx/Xxxx.vue
 *    PageScreen/xx/Aaaa.vue
 *
 *  导出为:
 *      export {
 *          Xxxx,
 *          Aaaa
 *      }
 *
 *  导入为:
 *     import modules  from '路径'
 *     const { Xxxx,Aaaa} = modules
 *
 */
const modulesFiles = import.meta.globEager('../PageScreen/**/*.vue');
const modules: Record<string, Component> = {};
Object.keys(modulesFiles).forEach(item => {
    const mod = modulesFiles[item];
    const ExportName = item.replace(/(.*\/)*([^.]+).*/gi, '$2');
    modules[ExportName] = defineComponent(mod.default);
});
export default modules;
