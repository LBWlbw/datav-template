import * as echarts from 'echarts/core';
// 绘制左侧面
const wid = 10;
const w1 = Math.sin(Math.PI / 6) * wid; //4
const w2 = Math.sin(Math.PI / 3) * wid; // 6.8
const snapHeight = wid / 2;
const CubeLeft = echarts.graphic.extendShape({
    shape: {
        x: 0,
        y: 0,
    },
    buildPath: function (ctx, shape) {
        const xAxisPoint = shape.xAxisPoint;
        const c0 = [shape.x, shape.y];
        const c1 = [shape.x - w2, shape.y];
        const c2 = [shape.x - w2, xAxisPoint[1]];
        const c3 = [shape.x, xAxisPoint[1]];
        ctx.moveTo(c0[0], c0[1]).lineTo(c1[0], c1[1]).lineTo(c2[0], c2[1]).lineTo(c3[0], c3[1]).closePath();
    },
});
// 绘制右侧面
const CubeRight = echarts.graphic.extendShape({
    shape: {
        x: 0,
        y: 0,
    },
    buildPath: function (ctx, shape) {
        const xAxisPoint = shape.xAxisPoint;
        const c1 = [shape.x, shape.y];
        const c2 = [shape.x, xAxisPoint[1]];
        const c3 = [shape.x + w1, xAxisPoint[1] - w2 + snapHeight];
        const c4 = [shape.x + w1, shape.y - w2 + snapHeight];
        ctx.moveTo(c1[0], c1[1]).lineTo(c2[0], c2[1]).lineTo(c3[0], c3[1]).lineTo(c4[0], c4[1]).closePath();
    },
});
// 绘制顶面
const CubeTop = echarts.graphic.extendShape({
    shape: {
        x: 0,
        y: 0,
    },
    buildPath: function (ctx, shape) {
        //
        const c1 = [shape.x, shape.y];
        const c2 = [shape.x + w1, shape.y - w2 + snapHeight]; //右点
        const c3 = [shape.x - w2 + w1, shape.y - w2 + snapHeight];
        const c4 = [shape.x - w2, shape.y];
        ctx.moveTo(c1[0], c1[1]).lineTo(c2[0], c2[1]).lineTo(c3[0], c3[1]).lineTo(c4[0], c4[1]).closePath();
    },
});
// 三个面图形
echarts.graphic.registerShape('CubeLeft', CubeLeft);
echarts.graphic.registerShape('CubeRight', CubeRight);
echarts.graphic.registerShape('CubeTop', CubeTop);

let xData = ['安监', '应急', '安防', '车辆', '环保','能耗'];
let yData = [150, 126, 260, 220, 184,122];

const monitoringPoints = (data: any) => {
    const defaultConfig = {
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow',
            },
            backgroundColor: 'rgba(9, 24, 48, 0.5)',
            borderColor: 'rgba(75, 253, 238, 0.4)',
            textStyle: {
                color: '#CFE3FC',
            },
            borderWidth: 1,
            formatter(params: any) {
                return (
                    `监测点数量<br/><span style='display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:rgba(13, 124, 245, 1)'></span>${params[0].name}：<span>${params[0].data}</span>`
                );
            }

        },
        grid: {
            top: '20%',
            left: '5%',
            bottom: '5%',
            right: '5%',
            containLabel: true,
        },
        xAxis: {
            type: 'category',
            data: xData,
            axisLine: {
                show: true,
                lineStyle: {
                    color: 'rgba(121, 184, 255, .4)',
                    width: 1,
                },
            },
            axisTick: {
                show: false,
            },
            axisLabel: {
                fontSize: 10,
                margin:10,
                color: '#ffffff',
            },
            splitLine: {
                show: false,

            },
        },
        yAxis: {
            name: '数量',
            type: 'value',
            nameTextStyle: {
                color: '#FFFFFF',
                fontSize: 10,
                padding:[0,30,0,0]
            },
            nameGap:10,
            axisLine: {
                show: true,
                lineStyle: {
                    color: 'rgba(121, 184, 255, .4)',
                    width: 1,
                },
            },
            axiosTick: {
                show: false,
            },
            axisLabel: {
                color: '#FFFFFF',
                fontSize: 10,
                margin: 5,
            },
            splitLine: {
                show: true,
                lineStyle: {
                    color: 'rgba(0, 142, 114, .1)',
                    width: 1,
                },
            },
        },
        series: [
            {
                type: 'bar',
                label: {
                    show: false,
                },
                tooltip: {
                    show: false,
                },
                itemStyle: {
                    color: 'transparent',
                },
                data: yData,
            },
            {
                type: 'custom',
                renderItem: (params, api) => {
                    const location = api.coord([api.value(0), api.value(1)]);
                    location[0] = location[0] + wid * 0;
                    const xlocation = api.coord([api.value(0), 0]);
                    xlocation[0] = xlocation[0] + wid * 0;
                    return {
                        type: 'group',
                        children: [
                            {
                                type: 'CubeLeft',
                                shape: {
                                    api,
                                    xValue: api.value(0),
                                    yValue: api.value(1),
                                    x: location[0],
                                    y: location[1],
                                    xAxisPoint: xlocation,
                                },
                                style: {
                                    fill: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                                        {
                                            offset: 0,
                                            color: 'rgba(13, 93, 185, 1)',
                                        },
                                        {
                                            offset: 1,
                                            color: '#059de6',
                                        },
                                    ]),
                                },
                            },
                            {
                                type: 'CubeRight',
                                shape: {
                                    api,
                                    xValue: api.value(0),
                                    yValue: api.value(1),
                                    x: location[0],
                                    y: location[1],
                                    xAxisPoint: xlocation,
                                },
                                style: {
                                    fill: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                                        {
                                            offset: 0,
                                            color: 'rgba(13, 93, 185, 1)',
                                        },
                                        {
                                            offset: 1,
                                            color: '#254193',
                                        },
                                    ]),
                                },
                            },
                            {
                                type: 'CubeTop',
                                shape: {
                                    api,
                                    xValue: api.value(0),
                                    yValue: api.value(1),
                                    x: location[0],
                                    y: location[1],
                                    xAxisPoint: xlocation,
                                },
                                style: {
                                    fill: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                                        {
                                            offset: 0,
                                            color: 'rgba(38, 147, 255, 1)',
                                        },
                                        {
                                            offset: 1,
                                            color: 'rgba(38, 147, 255, 1)',
                                        },
                                    ]),
                                },
                            },
                        ],
                    };
                },
                color: 'blue',
                data: yData,
            },
        ],
    };

    const opt = { ...defaultConfig };
    return opt;
};

export default {
    monitoringPoints
};
