import * as echarts from 'echarts/core';

const data1 = [
    {
        name: '06/17',
        value: 175.17
    },
    {
        name: '06/18',
        value: 148.35
    },
    {
        name: '06/19',
        value: 95.36
    },
    {
        name: '06/20',
        value: 56
    },
    {
        name: '06/21',
        value: 56
    },
    {
        name: '06/22',
        value: 56
    },
    {
        name: '06/23',
        value: 56
    }
];
const xAxisData: any[] = [];
const seriesData1: any[] = [];
data1.forEach(item => {
    xAxisData.push(item.name);
    seriesData1.push(item.value);
});
const wasteDischarge = (data: any) => {
    const defaultConfig = {
        grid: {
            top: '30',
            bottom: '24',
            left: '30',
            right: '10'
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            },
            backgroundColor: 'rgba(0,0,0,0.5)',
            borderColor: 'rgba(0,0,0,0.5)',

            textStyle: {
                color: '#FFF'
            },
            formatter(params: any) {
                const item = params[1];
                return `${params[0].marker + item.name}：${item.value}/m³`;
            }
        },
        xAxis: {
            data: xAxisData,
            axisTick: {
                show: false
            },
            axisLine: {
                show: true,
                lineStyle: {
                    color: 'rgba(121, 184, 255, 0.4)'
                }
            },
            axisLabel: {
                show: true,
                align: 'center',
                interval: 1,
                fontSize: 10,
                color: '#B7BDBF'
            },
            interval: 0
        },
        yAxis: {
            name: 'm³',
            nameTextStyle: { color: 'rgba(250,250,250,.5)', padding: [0, 0, 0, -30] },
            type: 'value',
            splitNumber: 4,
            axisLabel: {
                formatter: '{value}',
                color: 'rgba(250,250,250,.5)',
                fontSize: 12
            },
            axisLine: {
                show: true,
                lineStyle: {
                    color: 'rgba(121, 184, 255, 0.4)'
                }
            },
            splitLine: {
                show: true,
                lineStyle: {
                    color: 'rgba(0, 142, 114, .1)'
                }
            }
        },
        series: [
            {
                name: '柱顶部',
                type: 'pictorialBar',
                symbolSize: [12, 8],
                symbolOffset: [0, -4],
                z: 12,
                itemStyle: {
                    color: '#FFF'
                },
                label: {
                    show: false,
                    position: 'top',
                    fontSize: 16
                },
                symbolPosition: 'end',
                data: seriesData1
            },
            {
                name: '柱底部',
                type: 'pictorialBar',
                symbolSize: [12, 7],
                symbolOffset: [0, 2],
                z: 12,
                itemStyle: {
                    color: 'rgba(2,119,130,0.1)'
                },
                data: seriesData1
            },
            {
                type: 'bar',
                itemStyle: {
                    color(params: any) {
                        return new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                            {
                                offset: 0,
                                color: 'rgba(54,247,227,0.8)'
                            },
                            {
                                offset: 1,
                                color: 'rgba(2,119,130,.4)'
                            }
                        ]);
                    },
                    opacity: 0.8
                },
                label: {
                    show: true,
                    position: 'top',
                    fontSize: 12,
                    color: '#FFFFFF'
                },
                z: 3,
                silent: true,
                barWidth: 12,
                barGap: '-100%', // Make series be overlap
                data: seriesData1
            }
        ]
    };

    const opt = { ...defaultConfig };
    return opt;
};

export default {
    wasteDischarge
};
