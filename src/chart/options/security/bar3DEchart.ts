import * as echarts from 'echarts/core';

const bar3DEchart = (data: any) => {
    const datas = [30, 55, 30, 50, 40, 60, 40];
    const datas2 = [50, 80, 50, 28, 70, 60, 80];
    // eslint-disable-next-line no-return-assign
    const datas3 = datas.map((value: number, index: number) => ([...datas2][index] += value));
    const defaultConfig = {
        tooltip: {
            trigger: 'axis',

            backgroundColor: 'rgba(9, 24, 48, 0.8)',
            borderColor: 'rgba(75, 253, 238, 0.4)',
            textStyle: {
                color: '#CFE3FC'
            },
            borderWidth: 1,

            axisPointer: {
                type: 'shadow',
                textStyle: {
                    color: '#fff'
                }
            },
            formatter(params: any) {
                const [item0, item1] = params;
                return `${
                    item0.name
                }<br/>${`${item1.marker}驶出：${item1.value}`} <br/>${`${item0.marker}进入：${item0.value}`} `;
            }
        },
        legend: {
            show: true,
            top: '5',
            itemWidth: 12,
            itemHeight: 12,
            right: 0,
            textStyle: {
                fontSize: 12
            },
            data: [
                {
                    name: '进入',
                    textStyle: {
                        color: '#0D7CF5'
                    }
                },
                {
                    name: '驶出',
                    textStyle: {
                        color: '#24EEF5'
                    }
                }
            ]
        },
        grid: {
            left: '1%',
            right: '3%',
            bottom: '5%',
            top: '26%',
            containLabel: true
        },
        xAxis: {
            type: 'category',
            data: ['06/10', '06/11', '06/12', '06/13', '06/14', '06/15', '06/16'],
            axisTick: {
                show: false
            },
            axisLabel: {
                color: '#B7BDBF',
                fontSize: 12
            }
        },
        yAxis: {
            name: '车次',
            nameTextStyle: { color: '#B7BDBF', padding: [0, 0, 0, -30] },
            type: 'value',
            splitNumber: 4,
            axisLabel: {
                formatter: '{value}',
                color: '#B7BDBF',
                fontSize: 12
            },
            axisTick: {
                show: false
            },
            splitLine: {
                show: true,
                lineStyle: {
                    color: 'rgba(0,142,114,.3)',
                    type: 'dashed'
                }
            }
        },
        series: [
            {
                name: '进入',
                type: 'bar',
                stack: 'account',
                barWidth: 20,
                itemStyle: {
                    color: {
                        x: 0,
                        y: 0,
                        x2: 0,
                        y2: 1,
                        type: 'linear',
                        global: false,
                        colorStops: [
                            {
                                offset: 0,
                                color: '#0D7CF5'
                            },
                            {
                                offset: 1,
                                color: '#0D5DB9'
                            }
                        ]
                    }
                },
                label: {
                    show: true,
                    verticalAlign: 'middle',
                    color: '#ffffff',
                    fontSize: 12,
                    align: 'center'
                },
                data: datas
            },
            {
                name: '驶出',
                type: 'bar',
                stack: 'account',
                barWidth: 20,
                itemStyle: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                        { offset: 0, color: '#24EEF5' },
                        { offset: 1, color: '#16B3B9' }
                    ])
                },
                label: {
                    show: true,
                    verticalAlign: 'middle',
                    color: '#ffffff',
                    fontSize: 12,
                    align: 'center'
                },
                data: datas2
            },
            {
                z: 3,
                type: 'pictorialBar',
                symbolPosition: 'end',
                data: datas,
                symbol: 'diamond',
                symbolOffset: [0, '-50%'],
                symbolSize: [20, 10],
                symbolRotate: 0,
                itemStyle: {
                    borderWidth: 0,
                    color: '#2693FF'
                }
            },
            {
                z: 3,
                type: 'pictorialBar',
                symbolPosition: 'end',
                data: datas3,
                symbol: 'diamond',
                symbolOffset: [0, '-50%'],
                symbolSize: [20, 10],
                label: {
                    show: false
                },
                itemStyle: {
                    borderWidth: 0,
                    color: '#20F8FF'
                }
            }
        ]
    };

    const opt = { ...defaultConfig };
    return opt;
};

export default {
    bar3DEchart
};
