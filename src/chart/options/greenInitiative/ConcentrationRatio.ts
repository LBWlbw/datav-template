import * as echarts from 'echarts/core';

const barWidth = 3; /* 进度条及进度条radius宽度 */
const attaData: any[] = []; /* 进度条数据 */
const attaVal: any[] = []; /* 进度条数值 */
const topName: any[] = []; /* 进度条名称 */
let salvProMax: any[] = []; /* 背景条数据 */
const datas = [
    {
        name: 'SO2',
        value: 2400
    },
    {
        name: 'NO2',
        value: 1800
    },
    {
        name: 'PM2.5',
        value: 1400
    },
    {
        name: 'PM10',
        value: 1000
    },
    {
        name: 'CO',
        value: 600
    }
];
const attackSourcesColor = [
    new echarts.graphic.LinearGradient(0, 1, 1, 1, [
        { offset: 0, color: '#F78B9B' },
        { offset: 1, color: '#FC361C' }
    ]),
    new echarts.graphic.LinearGradient(0, 1, 1, 1, [
        { offset: 0, color: '#F7B68B' },
        { offset: 1, color: '#FC701C' }
    ]),
    new echarts.graphic.LinearGradient(0, 1, 1, 1, [
        { offset: 0, color: '#F7E48B' },
        { offset: 1, color: '#FCCF1C' }
    ]),
    new echarts.graphic.LinearGradient(0, 1, 1, 1, [
        { offset: 0, color: '#2DBAFB' },
        { offset: 1, color: '#58F3FD' }
    ])
];
datas.forEach((it: any, i) => {
    const itemStyle = {
        color: i > 3 ? attackSourcesColor[3] : attackSourcesColor[i]
    };
    topName[i] = `${it.name}`;
    attaVal[i] = it.value;
    attaData[i] = {
        value: parseFloat(it.value).toFixed(2),
        itemStyle
    };
});
/* 该值无具体作用，取进度最大值 * 1.2 */
salvProMax = Array(attaVal.length).fill(Math.max(...attaVal) * 1.2);
const concentrationRatio = (data: any) => {
    const defaultConfig = {
        tooltip: {
            show: true,
            textStyle: {
                fontSize: 14
            }
        },
        grid: {
            left: '28%',
            right: '5%',
            top: '7%',
            bottom: '0'
        },
        legend: {
            show: false
        },
        xAxis: {
            show: false
        },
        yAxis: [
            {
                // 名称
                type: 'category',
                inverse: true,
                axisTick: 'none',
                axisLine: 'none',
                show: true,
                axisLabel: {
                    color: '#1F4265',
                    formatter: (val: any, idxs: number): string => {
                        // eslint-disable-next-line no-return-assign
                        return `{num|No.${(idxs += 1)}}{name|${val}}`;
                    },
                    rich: {
                        num: {
                            width: 30,
                            fontSize: 12,
                            color: '#24BADF'
                        },
                        name: {
                            width: 45,
                            fontSize: 10,
                            color: '#FFFFFF'
                        }
                    }
                },
                data: topName
            }
        ],
        series: [
            // 进度条
            {
                zlevel: 1,
                name: '',
                type: 'bar',
                barWidth,
                data: attaData,
                align: 'center',
                itemStyle: {
                    borderRadius: barWidth
                }
            },
            // 背景条
            {
                name: '',
                type: 'bar',
                barWidth,
                barGap: '-100%',
                data: salvProMax,
                itemStyle: {
                    borderRadius: barWidth,
                    color: '#084C99'
                },
                tooltip: {
                    show: false
                }
            }
        ]
    };

    const opt = { ...defaultConfig };
    return opt;
};

export default {
    concentrationRatio
};
