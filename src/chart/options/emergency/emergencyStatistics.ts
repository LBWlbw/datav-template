import * as echarts from 'echarts/core';

// 绘制左侧面
const wid = 130;
const w1 = Math.sin(Math.PI / 6) * wid; // 4
const w2 = Math.sin(Math.PI / 3) * wid; // 6.8
const snapHeight = wid / 2;
const CubeLeft = echarts.graphic.extendShape({
    shape: {
        x: 0,
        y: 0
    },
    buildPath(ctx: any, shape) {
        const { xAxisPoint } = shape;
        const c0 = [shape.x, shape.y];
        const c1 = [shape.x - w2, shape.y];
        const c2 = [shape.x - w2, xAxisPoint[1]];
        const c3 = [shape.x, xAxisPoint[1]];
        ctx.moveTo(c0[0], c0[1]).lineTo(c1[0], c1[1]).lineTo(c2[0], c2[1]).lineTo(c3[0], c3[1]).closePath();
    }
});
// 绘制右侧面
const CubeRight = echarts.graphic.extendShape({
    shape: {
        x: 0,
        y: 0
    },
    buildPath(ctx: any, shape) {
        const { xAxisPoint } = shape;
        const c1 = [shape.x, shape.y];
        const c2 = [shape.x, xAxisPoint[1]];
        const c3 = [shape.x + w1, xAxisPoint[1] - w2 + snapHeight];
        const c4 = [shape.x + w1, shape.y - w2 + snapHeight];
        ctx.moveTo(c1[0], c1[1]).lineTo(c2[0], c2[1]).lineTo(c3[0], c3[1]).lineTo(c4[0], c4[1]).closePath();
    }
});
// 绘制顶面
const CubeTop = echarts.graphic.extendShape({
    shape: {
        x: 0,
        y: 0
    },
    buildPath(ctx: any, shape) {
        //
        const c1 = [shape.x, shape.y];
        const c2 = [shape.x + w1, shape.y - w2 + snapHeight]; // 右点
        const c3 = [shape.x - w2 + w1, shape.y - w2 + snapHeight];
        const c4 = [shape.x - w2, shape.y];
        ctx.moveTo(c1[0], c1[1]).lineTo(c2[0], c2[1]).lineTo(c3[0], c3[1]).lineTo(c4[0], c4[1]).closePath();
    }
});
// 三个面图形
echarts.graphic.registerShape('CubeLeft', CubeLeft);
echarts.graphic.registerShape('CubeRight', CubeRight);
echarts.graphic.registerShape('CubeTop', CubeTop);

const xData = ['救援物资', '救援力量', '消防措施', '应急专家'];
const yData = [150, 126, 260, 220, 184];

const emergencyStatistics = (data: any) => {
    const defaultConfig = {
        grid: {
            left: '5%',
            right: '5%',
            top: '12%',
            bottom: '10%',
            containLabel: true
        },
        xAxis: {
            type: 'category',
            data: xData,
            axisLine: {
                show: true,
                lineStyle: {
                    color: '#3e6f8e',
                    width: 1
                }
            },
            axisTick: {
                show: false,
                length: 9,
                alignWithLabel: false,
                lineStyle: {
                    color: '#AAA'
                }
            },
            axisLabel: {
                fontSize: 12,
                margin: 10,
                color: 'white'
            },
            splitLine: {
                show: false,
                lineStyle: {
                    color: '#ffffff',
                    opacity: 0.2,
                    width: 1
                }
            }
        },
        yAxis: {
            type: 'value',
            min: 0,
            axisLine: {
                show: false,
                lineStyle: {
                    color: '#7ebaf2'
                }
            },
            splitLine: {
                show: false
            },

            axisTick: {
                show: false
            },
            axisLabel: {
                show: false,
                fontSize: 12
            },
            boundaryGap: ['20%', '20%']
        },
        series: [
            {
                type: 'custom',
                renderItem: (params: any, api: any) => {
                    let cubeLeftStyle: any = '';
                    let cubeRightStyle: any = '';
                    let cubeTopStyle: any = '';
                    if (+params.dataIndex === 0) {
                        cubeLeftStyle = new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                            {
                                offset: 0,
                                color: '#0590FF'
                            },
                            {
                                offset: 1,
                                color: '#528FB7'
                            }
                        ]);
                        cubeRightStyle = new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                            {
                                offset: 0,
                                color: '#0590FF'
                            },
                            {
                                offset: 1,
                                color: '#76C7FB'
                            }
                        ]);
                        cubeTopStyle = new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                            {
                                offset: 0,
                                color: '#7BCCFF'
                            },
                            {
                                offset: 1,
                                color: '#7BCCFF'
                            }
                        ]);
                    } else if (+params.dataIndex === 1) {
                        cubeLeftStyle = new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                            {
                                offset: 0,
                                color: '#FF8005'
                            },
                            {
                                offset: 1,
                                color: '#B65F26'
                            }
                        ]);
                        cubeRightStyle = new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                            {
                                offset: 0,
                                color: '#FF8005'
                            },
                            {
                                offset: 1,
                                color: '#F07E38'
                            }
                        ]);
                        cubeTopStyle = new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                            {
                                offset: 0,
                                color: '#FD853A'
                            },
                            {
                                offset: 1,
                                color: '#FD853A'
                            }
                        ]);
                    } else if (+params.dataIndex === 2) {
                        cubeLeftStyle = new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                            {
                                offset: 0,
                                color: '#0590FF'
                            },
                            {
                                offset: 1,
                                color: '#528FB7'
                            }
                        ]);
                        cubeRightStyle = new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                            {
                                offset: 0,
                                color: '#0590FF'
                            },
                            {
                                offset: 1,
                                color: '#76C7FB'
                            }
                        ]);
                        cubeTopStyle = new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                            {
                                offset: 0,
                                color: '#7BCCFF'
                            },
                            {
                                offset: 1,
                                color: '#7BCCFF'
                            }
                        ]);
                    } else if (+params.dataIndex === 3) {
                        cubeLeftStyle = new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                            {
                                offset: 0,
                                color: '#FF8005'
                            },
                            {
                                offset: 1,
                                color: '#B65F26'
                            }
                        ]);
                        cubeRightStyle = new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                            {
                                offset: 0,
                                color: '#FF8005'
                            },
                            {
                                offset: 1,
                                color: '#F07E38'
                            }
                        ]);
                        cubeTopStyle = new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                            {
                                offset: 0,
                                color: '#FD853A'
                            },
                            {
                                offset: 1,
                                color: '#FD853A'
                            }
                        ]);
                    }
                    const location = api.coord([api.value(0), api.value(1)]);
                    location[0] += wid * 0;
                    const xlocation = api.coord([api.value(0), 0]);
                    xlocation[0] += wid * 0;
                    return {
                        type: 'group',
                        children: [
                            {
                                type: 'CubeLeft',
                                shape: {
                                    api,
                                    xValue: api.value(0),
                                    yValue: api.value(1),
                                    x: location[0],
                                    y: location[1],
                                    xAxisPoint: xlocation
                                },
                                style: {
                                    fill: cubeLeftStyle
                                }
                            },
                            {
                                type: 'CubeRight',
                                shape: {
                                    api,
                                    xValue: api.value(0),
                                    yValue: api.value(1),
                                    x: location[0],
                                    y: location[1],
                                    xAxisPoint: xlocation
                                },
                                style: {
                                    fill: cubeRightStyle
                                }
                            },
                            {
                                type: 'CubeTop',
                                shape: {
                                    api,
                                    xValue: api.value(0),
                                    yValue: api.value(1),
                                    x: location[0],
                                    y: location[1],
                                    xAxisPoint: xlocation
                                },
                                style: {
                                    fill: cubeTopStyle
                                }
                            }
                        ]
                    };
                },
                data: yData
            },
            {
                type: 'bar',
                label: {
                    normal: {
                        show: true,
                        position: 'top',
                        fontSize: 12,
                        color: '#fff',
                        offset: [0, -5]
                    }
                },
                itemStyle: {
                    color: 'transparent'
                },
                tooltip: {},
                data: yData
            }
        ]
    };

    const opt = { ...defaultConfig };
    return opt;
};

export default {
    emergencyStatistics
};
