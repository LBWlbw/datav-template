import * as echarts from 'echarts/core';

const chainGrowthRate = (data: any) => {
    const defaultConfig = {
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            },
            backgroundColor: 'rgba(9, 24, 48, 0.5)',
            borderColor: 'rgba(75, 253, 238, 0.4)',
            textStyle: {
                color: '#CFE3FC'
            },
            borderWidth: 1,
            formatter(params: any) {
                let str = '';
                for (let i = 0; i < params.length; i++) {
                    if (i === 0) {
                        str += `${params[i].name}<br/><span style='display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:rgba(36,207,233,0.9)'></span>${params[i].seriesName}：<span>${params[0].data}</span>%<br/>`;
                        // eslint-disable-next-line no-continue
                        continue;
                    }
                    str += `<span style='display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:rgba(13, 93, 185, 1)'></span>${params[i].seriesName}：<span>${params[i].data}</span>度<br/>`;
                }
                return str;
            }
        },
        legend: {
            show: false
        },
        grid: {
            left: '40',
            right: '40',
            top: '40',
            bottom: '30'
        },
        toolbox: {
            show: true,
            orient: 'vertical',
            x: 'right',
            y: 'center'
        },
        xAxis: [
            {
                type: 'category',
                boundaryGap: true,
                axisTick: {
                    show: false
                },
                data: ['1月', '2月', '3月', '4月', '5月', '6月'],
                axisLine: {
                    lineStyle: {
                        color: 'rgba(121, 184, 255, .4)'
                    }
                },
                axisLabel: {
                    interval: 0,
                    color: 'rgba(207, 227, 252, 1)',
                    fontSize: 10
                }
            }
        ],
        yAxis: [
            {
                type: 'value',
                name: '单位: 度',
                nameTextStyle: {
                    fontSize: 10,
                    color: 'rgba(207, 227, 252, 1)',
                    padding: [0, 20, 0, 0]
                },
                axisTick: {
                    show: false
                },
                axisLine: {
                    show: true,
                    lineStyle: {
                        color: 'rgba(121, 184, 255, .4)'
                    }
                },
                axisLabel: {
                    interval: 0,
                    color: 'rgba(207, 227, 252, 1)',
                    fontSize: 10
                },
                splitLine: {
                    show: false
                }
            },
            {
                type: 'value',
                name: '增速',
                nameTextStyle: {
                    fontSize: 10,
                    color: 'rgba(207, 227, 252, 1)',
                    padding: [0, 0, 0, 30]
                },
                axisTick: {
                    show: false
                },
                axisLine: {
                    show: false
                },
                min: 0,
                axisLabel: {
                    interval: 0,
                    color: 'rgba(207, 227, 252, 1)',
                    fontSize: 10,
                    formatter: '{value} %'
                },
                splitLine: {
                    show: false
                }
            }
        ],
        series: [
            {
                name: '增速',
                yAxisIndex: 1,
                type: 'line',
                smooth: false,
                symbol: 'circle', // 标记的图形为实心圆
                symbolSize: 4, // 标记的大小
                data: [30, 50, 53, 42, 66, 71],
                itemStyle: {
                    color: 'rgba(40, 190, 198, 1)',
                    borderColor: 'rgba(255, 255, 255, 1)', // 圆点透明 边框
                    borderWidth: 1
                },
                lineStyle: {
                    color: 'rgba(40, 190, 198, 1)'
                }
            },
            {
                type: 'bar',
                yAxisIndex: 0,
                name: '度数',
                itemStyle: {
                    color: new echarts.graphic.LinearGradient(
                        0,
                        0,
                        0,
                        1,
                        [
                            {
                                offset: 0,
                                color: 'rgba(13, 93, 185, 1)'
                            },
                            {
                                offset: 1,
                                color: 'rgba(32, 178, 232, 0)'
                            }
                        ],
                        false
                    )
                },
                barWidth: 12,
                data: [220, 330, 110, 120, 140, 150]
            }
        ]
    };

    const opt = { ...defaultConfig };
    return opt;
};

export default {
    chainGrowthRate
};
