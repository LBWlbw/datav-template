import { provide, inject } from 'vue';
import type { InjectionKey } from 'vue';

/** 创建共享上下文状态 */
export default function useContext<T>(contextName = 'context') {
    const injectKey = contextName;

    function useProvide(context: T) {
        provide(injectKey, context);
        console.log(contextName, 'contextName');
        console.log(context, 'context');
    }

    function useInject() {
        console.log(inject(injectKey), 'inject(injectKey)');
        return inject(injectKey) as T;
    }

    return {
        useProvide,
        useInject
    };
}
