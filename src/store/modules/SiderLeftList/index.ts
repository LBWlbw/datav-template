import { defineStore } from 'pinia';

interface SiderLeftListDataStaus {
    /** 当前视图 */
    SiderLeftListData: Array<ViewSider.SiderLeftListDataInfo>;
    /** 收起/展开 */
    ShowLeft: boolean;
}

export const SiderLeftListStore = defineStore('SiderLeftListStore', {
    state: (): SiderLeftListDataStaus => ({
        SiderLeftListData: [],
        ShowLeft: true
    }),
    actions: {
        /** 更新当前视图 */
        SetListData(item: Array<ViewSider.SiderLeftListDataInfo>) {
            this.SiderLeftListData = item;
        },
        /** 显示隐藏 */
        UpDateShowList() {
            this.ShowLeft = !this.ShowLeft;
        }
    }
});
