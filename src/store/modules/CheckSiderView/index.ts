import { defineStore } from 'pinia';

interface CheckSiderViewState {
    /** 当前视图 */
    ViewStatus: ViewSider.ViewStatus;
    /** 是否显示 */
    ShowStatus: boolean;
}

export const CheckSiderView = defineStore('CheckSiderView', {
    state: (): CheckSiderViewState => ({
        ViewStatus: 'OverView',
        ShowStatus: true
    }),
    actions: {
        /** 更新当前视图 */
        UpDateViewStatus(payload: ViewSider.ViewStatus) {
            this.ViewStatus = payload;
        },
        /** 显示 */
        UpDateShowViews() {
            this.ShowStatus = true;
        },
        /** 隐藏 */
        UpDateHideViews() {
            this.ShowStatus = false;
        },
        /** 连续点击 */
        UpDateToggleView() {
            this.ShowStatus = !this.ShowStatus;
        }
    }
});
