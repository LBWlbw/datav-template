import * as echarts from 'echarts/core';

const WastewaterDischarge = (data: any) => {
    const { unit, xVal } = data;
    const xAxisData: any[] = [];
    const seriesData1: any[] = [];
    let sum = 0;
    xVal.forEach((item: any) => {
        xAxisData.push(item.name);
        seriesData1.push(item.value);
        sum += item.value;
    });

    const defaultConfig = {
        grid: {
            top: '30',
            bottom: '24',
            left: '30',
            right: '10'
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            },
            backgroundColor: 'rgba(0,0,0,0.5)',
            borderColor: 'rgba(0,0,0,0.5)',

            textStyle: {
                color: '#FFF'
            },
            formatter(params: any) {
                const item = params[1];
                return `${params[0].marker + item.name}：${item.value}/${unit}`;
            }
        },
        xAxis: {
            data: xAxisData,
            axisTick: {
                show: false
            },
            axisLine: {
                show: true,
                lineStyle: {
                    color: 'rgba(121, 184, 255, 0.4)'
                }
            },
            axisLabel: {
                show: true,
                align: 'center',
                interval: 0,
                fontSize: 10,
                color: '#B7BDBF'
            },
            interval: 0
        },
        yAxis: {
            name: unit,
            nameTextStyle: { color: 'rgba(250,250,250,.5)', padding: [0, 0, 0, -30] },
            type: 'value',
            splitNumber: 4,
            axisLabel: {
                formatter: '{value}',
                color: 'rgba(250,250,250,.5)',
                fontSize: 12
            },
            axisLine: {
                show: true,
                lineStyle: {
                    color: 'rgba(121, 184, 255, 0.4)'
                }
            },
            splitLine: {
                show: true,
                lineStyle: {
                    color: 'rgba(0, 142, 114, .1)'
                }
            }
        },
        series: [
            {
                name: '柱顶部',
                type: 'pictorialBar',
                symbolSize: [16, 8],
                symbolOffset: [0, -4],
                z: 12,
                itemStyle: {
                    color: 'rgba(33, 243, 255, 0.7)'
                },
                label: {
                    show: false,
                    position: 'top',
                    fontSize: 16
                },
                symbolPosition: 'end',
                data: seriesData1
            },
            {
                name: '柱底部',
                type: 'pictorialBar',
                symbolSize: [16, 7],
                symbolOffset: [0, 2],
                z: 12,
                itemStyle: {
                    color: 'rgba(31, 97, 234, 0.4)'
                },
                data: seriesData1
            },
            {
                showBackground: true,
                backgroundStyle: {
                    color: 'rgba(6,78,141,.46)'
                },
                type: 'bar',
                itemStyle: {
                    color(params: any) {
                        return new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                            {
                                offset: 1,
                                color: 'rgba(34, 68, 172, 0.35)'
                            },
                            {
                                offset: 0,
                                color: 'rgba(0, 183, 255, 1)'
                            }
                        ]);
                    },
                    opacity: 0.8
                },
                label: {
                    show: false,
                    position: 'top',
                    fontSize: 12,
                    color: '#FFFFFF'
                },
                z: 3,
                silent: true,
                barWidth: 16,
                barGap: '-100%', // Make series be overlap
                data: seriesData1
            }
        ]
    };

    const opt = { ...defaultConfig };
    return opt;
};

export default {
    WastewaterDischarge
};
