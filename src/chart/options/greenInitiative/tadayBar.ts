import * as echarts from 'echarts/core';

const data1 = [
    {
        name: '8:00',
        value: 175.17
    },
    {
        name: '9:00',
        value: 148.35
    },
    {
        name: '10:00',
        value: 95.36
    },
    {
        name: '11:00',
        value: 56
    },
    {
        name: '12:00',
        value: 45
    }
];
const xAxisData: any[] = [];
const seriesData1: any[] = [];
let sum = 0;
data1.forEach(item => {
    xAxisData.push(item.name);
    seriesData1.push(item.value);
    sum += item.value;
});

const tadayBar = ({ chartViewStatus }: any) => {
    let grid = {
        top: '30',
        bottom: '16',
        left: '30',
        right: '10'
    };
    if (chartViewStatus === 'Statistics') {
        grid = {
            top: '16%',
            bottom: '28%',
            left: '30',
            right: '10'
        };
    }
    console.log(grid, 'grid');
    const defaultConfig = {
        grid,
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            },
            backgroundColor: 'rgba(0,0,0,0.5)',
            borderColor: 'rgba(0,0,0,0.5)',

            textStyle: {
                color: '#FFF'
            },
            formatter(params: any) {
                const item = params[1];
                return `${params[0].marker + item.name}：${item.value} 万/m³`;
            }
        },
        xAxis: {
            data: xAxisData,
            axisTick: {
                show: false
            },
            axisLine: {
                show: true,
                lineStyle: {
                    color: 'rgba(0, 142, 114, 0.1)'
                }
            },
            axisLabel: {
                show: true,
                align: 'center',
                fontSize: 10,
                color: '#B7BDBF'
            },
            interval: 0
        },
        yAxis: {
            name: '万/m³',
            nameTextStyle: { color: 'rgba(250,250,250,.5)', padding: [0, 0, 0, -30] },
            type: 'value',
            splitNumber: 4,
            axisLabel: {
                formatter: '{value}',
                color: 'rgba(250,250,250,.5)',
                fontSize: 12
            },
            axisLine: {
                lineStyle: {
                    color: 'rgba(255,255,255,0.45)'
                }
            },
            axisTick: {
                show: false
            },
            splitLine: {
                show: true,
                lineStyle: {
                    color: 'rgba(255,255,255,0.15)',
                    type: 'dashed'
                }
            }
        },
        series: [
            {
                name: '柱顶部',
                type: 'pictorialBar',
                symbolSize: [18, 10],
                symbolOffset: [0, -5],
                z: 12,
                itemStyle: {
                    color: 'rgba(33, 243, 255, 0.7)'
                },
                label: {
                    show: false,
                    position: 'top',
                    fontSize: 16
                },
                symbolPosition: 'end',
                data: seriesData1
            },
            {
                name: '柱底部',
                type: 'pictorialBar',
                symbolSize: [18, 10],
                symbolOffset: [0, 5],
                z: 12,
                itemStyle: {
                    color: 'rgba(31, 97, 234, 0.4)'
                },
                data: seriesData1
            },
            {
                type: 'bar',
                itemStyle: {
                    color(params: any) {
                        return new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                            {
                                offset: 1,
                                color: 'rgba(34, 68, 172, 0.35)'
                            },
                            {
                                offset: 0,
                                color: 'rgba(0, 183, 255, 1)'
                            }
                        ]);
                    },
                    opacity: 0.8
                },
                label: {
                    show: false,
                    position: 'top',
                    fontSize: 12,
                    color: '#FFFFFF'
                },
                z: 3,
                silent: true,
                barWidth: 18,
                barGap: '-100%', // Make series be overlap
                data: seriesData1
            }
        ]
    };

    const opt = { ...defaultConfig };
    return opt;
};

export default {
    tadayBar
};
