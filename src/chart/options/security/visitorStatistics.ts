/** 近7天访客统计 */
import * as echarts from 'echarts/core';

const visitorStatistics = (data: any) => {
    const defaultConfig = {
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                lineStyle: {
                    color: 'rgba(8, 195, 171, 1)'
                }
            }
        },
        legend: {
            show: false
        },
        grid: {
            top: '20%',
            left: '10%',
            right: '5%',
            bottom: '14%'
        },
        xAxis: {
            type: 'category',
            data: ['06/17', '06/18', '06/19', '06/20', '06/21'],
            axisLabel: {
                color: '#B7BDBF',
                fontSize: 10
            },
            axisTick: {
                //坐标轴刻度相关设置。
                show: false
            },
            axisLine: {
                show: false
            },
            splitLine: {
                //坐标轴在 grid 区域中的分隔线。
                show: false
            }
        },
        yAxis: [
            {
                name: '人',
                nameTextStyle: {
                    fontSize: 10,
                    color: '#B7BDBF',
                    padding: [20, 20, 0, 0]
                },
                type: 'value',
                axisLabel: {
                    color: '#B7BDBF',
                    fontSize: 10
                },
                axisLine: {
                    show: false
                },
                axisTick: {
                    show: false
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        type: 'dashed',
                        color: 'rgba(0, 142, 114, 0.33)'
                    }
                }
            }
        ],
        series: [
            {
                name: '访客',
                type: 'line',
                smooth: true,
                symbol: 'circle',
                symbolSize: 8,
                itemStyle: {
                    color: 'rgba(8, 195, 171, 1)',
                    shadowBlur: 20,
                    borderColor: 'rgba(255, 255, 255, 1)',
                    borderWidth: 1
                },
                areaStyle: {
                    color: new echarts.graphic.LinearGradient(1, 0, 0, 0, [
                        {
                            offset: 0,
                            color: 'rgba(0, 104, 220, 0.5)'
                        },
                        {
                            offset: 1,
                            color: 'rgba(0, 227, 192, 0.5)'
                        }
                    ])
                },
                lineStyle: {
                    color: new echarts.graphic.LinearGradient(1, 0, 0, 0, [
                        {
                            offset: 0,
                            color: 'rgba(0, 104, 220, 0.5)'
                        },
                        {
                            offset: 1,
                            color: 'rgba(0, 227, 192, 0.5)'
                        }
                    ])
                },
                data: [150, 100, 200, 99, 100]
            }
        ]
    };

    const opt = { ...defaultConfig };
    return opt;
};

export default {
    visitorStatistics
};
