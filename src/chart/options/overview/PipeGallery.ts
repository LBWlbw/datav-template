import * as echarts from 'echarts/core';

const data1 = [50, 60, 70, 20, 30, 10];
const className = ['供电', '供水', '天然气', '雨水', '污水', '危化品'];
const barTopColor = ['#23FFFC', '#23B9FF', '#23FFFC', '#23B9FF', '#23FFFC', '#23B9FF', '#23FFFC', '#23B9FF'];
const barBottomColor = [
    'rgba(35, 255, 252, 0.32)',
    'rgba(35, 185, 255, 0.32)',

    'rgba(35, 255, 252, 0.32)',
    'rgba(35, 185, 255, 0.32)',

    'rgba(35, 255, 252, 0.32)',
    'rgba(35, 185, 255, 0.32)',

    'rgba(35, 255, 252, 0.32)',
    'rgba(35, 185, 255, 0.32)'
];
const pipegalleryStatistics = (data: any) => {
    const defaultConfig = {
        legend: {
            show: false
        },
        grid: {
            left: '0',
            right: '0',
            bottom: '-20',
            top: '0',
            containLabel: true
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            },
            formatter(params: any) {
                return (
                    `${params[0].name}<br/>` +
                    `<span style='display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:rgba(36,207,233,0.9)'></span>${
                        // params[0].seriesName + ' : ' + Number((params[0].value.toFixed(4) / 10000).toFixed(2)).toLocaleString() + ' <br/>'
                        params[0].seriesName
                    } : ${params[0].value}`
                );
            }
        },
        xAxis: {
            show: false,
            type: 'value'
        },
        yAxis: [
            {
                type: 'category',
                inverse: true,
                axisLabel: {
                    show: true,
                    fontSize: 12,
                    color: '#B7BDBF'
                },
                splitLine: {
                    show: false
                },
                axisTick: {
                    show: false
                },
                axisLine: {
                    show: false
                },
                data: className
            },
            {
                type: 'category',
                inverse: true,
                axisTick: 'none',
                axisLine: 'none',
                show: true,
                axisLabel: {
                    color: '#ffffff',
                    fontSize: '10',
                    formatter(value: any) {
                        return `${value} m`;
                    }
                },
                data: data1
            }
        ],
        series: [
            {
                name: '数量',
                type: 'bar',
                zlevel: 1,
                itemStyle: {
                    borderRadius: 0,
                    color(params: any) {
                        return new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                            {
                                offset: 1,
                                color: barTopColor[params.dataIndex]
                            },
                            {
                                offset: 0,
                                color: barBottomColor[params.dataIndex]
                            }
                        ]);
                    },
                    borderColor(params: any) {
                        return barTopColor[params.dataIndex];
                    }
                },
                showBackground: true,
                backgroundStyle: {
                    color: 'rgba(0, 102, 255, 0.1)'
                },
                barWidth: 11,
                data: data1
            }
        ]
    };

    const opt = { ...defaultConfig };
    return opt;
};

export default {
    pipegalleryStatistics
};
