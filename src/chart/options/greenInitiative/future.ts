import * as echarts from 'echarts/core';

// 条形柱状图
const seriesBar = [
    {
        name: '设备在线率',
        type: 'bar',
        showBackground: true,
        backgroundStyle: {
            color: 'rgba(3,33,58,0.8)'
        },
        barWidth: 16,
        itemStyle: {
            label: {
                show: false, // 开启显示
                position: 'top', // 在上方显示
                textStyle: {
                    // 数值样式
                    color: '#fff',
                    fontSize: 16
                }
            },
            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                {
                    offset: 0,
                    color: 'rgba(146, 225, 255, 1)'
                },
                {
                    offset: 1,
                    color: 'rgba(0, 151, 251, 1)'
                }
            ])
        },
        data: [] as number[],
        z: 10,
        zlevel: 0
    },
    {
        // 分隔
        type: 'pictorialBar',
        itemStyle: {
            color: 'rgba(3,33,58,0.8)'
        },
        symbolRepeat: 'fixed',
        symbolMargin: 4,
        symbol: 'rect',
        symbolClip: true,
        symbolSize: [18, 2],
        symbolPosition: 'start',
        data: [] as number[],
        width: 2,
        z: 0,
        zlevel: 1
    },

    {
        name: '背影',
        type: 'line',
        smooth: true, // 平滑曲线显示
        showAllSymbol: false, // 显示所有图形。
        symbolSize: 0,
        lineStyle: {
            width: 0
        },
        areaStyle: {
            color: 'rgba(0, 151, 251, 0.1)'
        },
        data: [] as number[],
        z: 5
    }
];
// 折线面积图
const seriesLine = [
    {
        type: 'line',
        symbolSize: 0,
        smooth: true,
        lineStyle: {
            width: 3,
            color: new echarts.graphic.LinearGradient(
                0,
                0,
                0,
                1,
                [
                    {
                        offset: 0,
                        color: 'rgba(0,104,220,.3)'
                    },
                    {
                        offset: 1,
                        color: 'rgba(0,227,192,.3)'
                    }
                ],
                false
            ),
            borderColor: new echarts.graphic.LinearGradient(
                0,
                0,
                0,
                1,
                [
                    {
                        offset: 0,
                        color: 'rgba(0,104,220,.3)'
                    },
                    {
                        offset: 1,
                        color: 'rgba(0,227,192,.3)'
                    }
                ],
                false
            )
        },

        areaStyle: {
            // 区域填充样式
            color: new echarts.graphic.LinearGradient(
                0,
                0,
                0,
                1,
                [
                    {
                        offset: 0,
                        color: 'rgba(0,104,220,.3)'
                    },
                    {
                        offset: 1,
                        color: 'rgba(0,227,192,.3)'
                    }
                ],
                false
            ),
            shadowColor: 'rgba(25,163,223, 0.5)', // 阴影颜色
            shadowBlur: 0 // shadowBlur设图形阴影的模糊大小。配合shadowColor,shadowOffsetX/Y, 设置图形的阴影效果。
        },
        data: [] as number[]
    }
];
interface SeriesList {
    name: string;
    value: number;
}
interface DataProps {
    type: 'future1' | 'future1';
    seriesList: Array<SeriesList>;
    isStatistics: {
        [key in string]: any;
    };
}

/** 未来24小时图表
 *  - type: future1 | future1
 *  - seriesList: 图表值
 *  - isStatistics: 是否统计页面
 * */
const future1 = ({ type, seriesList, isStatistics = {} }: DataProps) => {
    const xAxisData: string[] = [];
    const seriesData: number[] = [];
    let gridVal = {};
    let series: any[] = [];
    const isBar = type === 'future1';
    // 统计页面需要自定义图表位置
    if (Object.keys(isStatistics).length) {
        gridVal = {
            top: isStatistics?.top,
            bottom: isStatistics?.bottom,
            left: isStatistics?.left,
            right: isStatistics?.right
        };
    } else {
        gridVal = {
            top: '20%',
            bottom: '22%',
            left: '30',
            right: isBar ? '10' : '6%'
        };
    }
    seriesList.forEach(item => {
        xAxisData.push(item.name);
        seriesData.push(item.value);
    });

    /* 条形柱状图 */
    if (isBar) {
        for (let i = 0; i < seriesBar.length; i++) {
            seriesBar[i].data = seriesData;
        }
        series = seriesBar;
    } else {
        // 面积折线图
        seriesLine[0].data = seriesData;
        series = seriesLine;
    }
    const defaultConfig = {
        animation: true,
        grid: gridVal,
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: isBar ? 'shadow' : 'cross'
            },
            backgroundColor: 'rgba(0,0,0,0.5)',
            borderColor: 'rgba(0,0,0,0.5)',

            textStyle: {
                color: '#FFF'
            },
            formatter: isBar
                ? (params: any) => {
                      const item = params[1];
                      return `${params[0].marker + item.name}：${item.value}`;
                  }
                : ''
        },
        xAxis: {
            data: xAxisData,
            boundaryGap: isBar,
            axisLine: {
                show: true // 隐藏X轴轴线
            },
            axisTick: {
                show: false // 隐藏X轴轴线
            },
            splitLine: {
                show: false,
                lineStyle: {
                    color: 'rgba(77, 128, 254, 0.2)',
                    width: 2
                }
            },
            axisLabel: {
                color: 'rgba(250,250,250,.5)',
                fontSize: 12
            }
        },
        yAxis: [
            {
                type: 'value',
                name: 'mg/m³',
                nameTextStyle: { color: 'rgba(250,250,250,.5)', padding: [0, 0, 0, -20] },
                splitLine: {
                    show: false,
                    lineStyle: {
                        color: 'rgba(77, 128, 254, 0.2)',
                        width: 2
                    }
                },
                axisTick: {
                    show: false
                },
                axisLine: {
                    show: true,
                    lineStyle: {
                        color: 'rgba(77, 128, 254, 0.2)'
                    }
                },
                axisLabel: {
                    formatter: '{value}',
                    color: 'rgba(250,250,250,.5)',
                    fontSize: 12
                }
            }
        ],
        series
    };

    const opt = { ...defaultConfig };
    return opt;
};

export default {
    future1
};
