import * as echarts from 'echarts/core';

const tankproperties = (data: any) => {
    const defaultConfig = {
        tooltip: {
            trigger: 'axis',
            backgroundColor: 'rgba(0,0,0,0.3)',
            borderColor: 'rgba(0,0,0,0.3)',

            textStyle: {
                color: '#FFF'
            },

            axisPointer: {
                type: 'line'
            }
        },
        grid: {
            top: '20%',
            left: '10',
            right: '10%',
            bottom: '18%',
            containLabel: true
        },
        xAxis: [
            {
                type: 'category',
                axisLine: {
                    show: false,
                    lineStyle: { color: '#dddddd' }
                },
                axisLabel: { color: '#FFFFFF', fontSize: 12 ,margin: 14, align: 'center',},
                splitLine: { show: false },
                axisTick: { show: false },
                boundaryGap: false,
                data: ['5/23 00:00', '6/3 00:00', '6/14 00:00', '6/22 00:00']
            }
        ],

        yAxis: [
            {
                name: '液位（mm）',
                nameTextStyle: { color: 'rgba(250,250,250,.5)', padding: [10, 0, 2, 12] },
                type: 'value',
                splitNumber: 4,
                axisLabel: {
                    formatter: '{value}',

                    textStyle: {
                        color: '#FFFFFF',
                        fontSize: 12
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: 'rgba(255,255,255,0.45)'
                    }
                },
                axisTick: {
                    show: false
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: 'rgba(255,255,255,0.2)',
                    }
                }
            }
        ],
        series: [
            {
                name: '数量',
                type: 'line',
                showAllSymbol: true,
                symbol: 'circle',
                symbolSize: 10,
                lineStyle: { normal: { color: '#1B93FB', width: 3 } },

                itemStyle: {
                    color: 'rgba(27, 147, 251, 1)',
                },

                areaStyle: {
                    normal: {
                        color: new echarts.graphic.LinearGradient(
                            0,
                            0,
                            0,
                            1,
                            [
                                { offset: 0, color: 'rgba(49,122,255,.3)' },
                                { offset: 1, color: 'rgba(159, 88, 255, 0)' }
                            ],
                            false
                        )
                    }
                },
                data: [210, 450, 680, 210]
            }
        ]
    };

    const opt = { ...defaultConfig };
    return opt;
};

export default {
    tankproperties
};
