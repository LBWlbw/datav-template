import * as echarts from 'echarts/core';

const combustible = (data: any) => {
    const defaultConfig = {
        tooltip: {
            trigger: 'axis',
            backgroundColor: 'rgba(0,0,0,0.3)',
            borderColor: 'rgba(0,0,0,0.3)',

            textStyle: {
                color: '#FFF'
            },

            axisPointer: {
                type: 'line'
            }
        },
        grid: {
            top: '20%',
            left: '6%',
            right: '10%',
            bottom: '18%'
        },
        xAxis: [
            {
                type: 'category',
                axisLine: {
                    show: false,
                    lineStyle: { color: '#dddddd' }
                },
                axisLabel: { color: '#FFFFFF', fontSize: 12, margin: 14 },
                splitLine: { show: false },
                axisTick: { show: false },
                boundaryGap: false,
                data: ['6/22', '6/23', '6/24', '6/25', '6/26', '6/27']
            }
        ],

        yAxis: [
            {
                name: '可燃气（%LEL）',
                nameTextStyle: { color: 'rgba(250,250,250,.5)', padding: [10, 0, 2, 30] },
                type: 'value',
                splitNumber: 4,
                axisLabel: {
                    formatter: '{value}',
                    textStyle: {
                        color: '#FFFFFF',
                        fontSize: 12
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: 'rgba(255,255,255,0.45)'
                    }
                },
                axisTick: {
                    show: false
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: 'rgba(255,255,255,0.2)'
                    }
                }
            }
        ],
        series: [
            {
                name: '数量',
                type: 'line',
                showAllSymbol: true,
                symbol: 'circle',
                symbolSize: 10,
                lineStyle: { normal: { color: '#1B93FB', width: 3 } },

                itemStyle: {
                    color: 'rgba(27, 147, 251, 1)'
                },

                areaStyle: {
                    normal: {
                        color: new echarts.graphic.LinearGradient(
                            0,
                            0,
                            0,
                            1,
                            [
                                { offset: 0, color: 'rgba(49,122,255,.3)' },
                                { offset: 1, color: 'rgba(159, 88, 255, 0)' }
                            ],
                            false
                        )
                    }
                },
                data: [210, 150, 380, 210, 222, 422, 212]
            }
        ]
    };

    const opt = { ...defaultConfig };
    return opt;
};

export default {
    combustible
};
