import * as echarts from 'echarts/core';
import { Scatter3DChart } from 'echarts-gl/charts';
import { Grid3DComponent } from 'echarts-gl/components';
import {
    BarChart,
    PieChart,
    LineChart,
    ScatterChart,
    GaugeChart,
    PictorialBarChart,
    FunnelChart,
    CustomChart
} from 'echarts/charts';
import {
    TitleComponent,
    TooltipComponent,
    ToolboxComponent,
    GridComponent,
    LegendComponent,
    DataZoomComponent,
    PolarComponent,
    GraphicComponent
} from 'echarts/components';
import { CanvasRenderer } from 'echarts/renderers';

// 注册组件
echarts.use([
    TitleComponent,
    TooltipComponent,
    GridComponent,
    BarChart,
    PieChart,
    CanvasRenderer,
    ToolboxComponent,
    LegendComponent,
    PolarComponent,
    LineChart,
    PictorialBarChart,
    ScatterChart,
    GaugeChart,
    FunnelChart,
    DataZoomComponent,
    Scatter3DChart,
    Grid3DComponent,
    GraphicComponent,
    CustomChart
]);

export default echarts;
