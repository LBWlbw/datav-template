/** 各卡口访客出入数量及占比 */
import * as echarts from 'echarts/core';

import imgSrc from '@/assets/image/imgSrc.png';

const visitorTraffic = (data: any) => {
    const defaultConfig = {
        color: ['rgba(0,162,255,0.8)', 'rgba(0,244,255,0.8)', 'rgba(255,237,0,0.8)'],

        grid: {
            bottom: 20,
            left: 20,
            right: '5%'
        },
        graphic: {
            //图形中间图片
            elements: [
                {
                    type: 'image',
                    style: {
                        image: imgSrc,
                        width: 70,
                        height: 70
                    },
                    left: 'center',
                    top: 'center'
                }
            ]
        },
        tooltip: {
            show: true,
            extraCssText: 'width:140px;'
        },
        series: [
            {
                radius: ['40%', '80%'],
                center: ['50%', '50%'],
                type: 'pie',
                label: {
                    show: true,
                    formatter: '{b}:{c}',
                    fontSize: 30,
                    position: 'outside'
                },
                labelLine: {
                    show: false
                },
                name: '访客出入数量',
                data: [
                    {
                        name: '道闸1',
                        value: 675
                    },
                    {
                        name: '道闸2',
                        value: 1397
                    },
                    {
                        name: '道闸3',
                        value: 318
                    }
                ]
            },
            {
                radius: ['30%', '55%'],
                center: ['50%', '50%'],
                emphasis: {
                    scale: false
                },
                type: 'pie',
                label: {
                    show: false
                },
                labelLine: {
                    show: false
                },
                animation: false,
                tooltip: {
                    show: false
                },
                data: [
                    {
                        value: 1,
                        itemStyle: {
                            color: 'rgba(25,41,56,0.2)'
                        }
                    }
                ]
            }
        ]
    };

    const opt = { ...defaultConfig };
    return opt;
};

export default {
    visitorTraffic
};
